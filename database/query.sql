

create database GymManagement;

CREATE TABLE users(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE employees (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(255) NOT NULL,
    salary INT(10) NOT NULL
);

-- CREATE TABLE trainer(
--     id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
--     EmployeeId INT(10) NOT NULL,
--     FOREIGN KEY (EmployeeId) REFERENCES employees(id),
--     name VARCHAR(100) NOT NULL,
--     address VARCHAR(255) NOT NULL,
--     contact INT(15) NOT NULL
-- )


CREATE TABLE members(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    address VARCHAR(255) NOT NULL,
    contact INT(15) NOT NULL
);


CREATE TABLE packages(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100) NOT NULL,
    duration INT(10) NOT NULL,
    price INT(10) NOT NULL
);


CREATE TABLE MemberPackageAssociation(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    MemberId INT(10) NOT NULL,
    FOREIGN KEY (MemberId) REFERENCES members(id),
    PackageId INT(10) NOT NULL,
    FOREIGN KEY (PackageId) REFERENCES packages(id)
);
